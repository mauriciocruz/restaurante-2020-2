package br.ucsal.bes20202.testequalidade.restaurante.domain;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20202.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;

public class Comanda {

	private Integer codigo;

	private Mesa mesa;

	private Map<Item, Integer> itens = new HashMap<>();

	private SituacaoComandaEnum situacao = SituacaoComandaEnum.ABERTA;

	public Comanda(Mesa mesa) throws MesaOcupadaException {
		super();
		verificarSituacaoMesa(mesa);
		this.mesa = mesa;
	}

	public void incluirItem(Item item, Integer qtd) throws ComandaFechadaException {
		Integer qtdAtual = 0;
		verificarComandaLivre();
		if (itens.containsKey(item)) {
			qtdAtual = itens.get(item);		}
		qtdAtual += qtd;
		itens.put(item, qtdAtual);
	}

	public Double fechar() {
		situacao = SituacaoComandaEnum.FECHADA;
		return calcularTotal();
	}

	public Integer getCodigo() {
		return codigo;
	}

	public Mesa getMesa() {
		return mesa;
	}

	private void verificarSituacaoMesa(Mesa mesa) throws MesaOcupadaException {
		if (mesa.getSituacao().equals(SituacaoMesaEnum.OCUPADA)) {
			throw new MesaOcupadaException(mesa.getNumero());
		}
	}

	private void verificarComandaLivre() throws ComandaFechadaException {
		if (situacao.equals(SituacaoComandaEnum.FECHADA)) {
			throw new ComandaFechadaException(codigo);
		}
	}

	private Double calcularTotal() {
		Double total = 0d;
		for (Item item : itens.keySet()) {
			total += item.getValorUnitario() * itens.get(item);
		}
		return total;
	}

}

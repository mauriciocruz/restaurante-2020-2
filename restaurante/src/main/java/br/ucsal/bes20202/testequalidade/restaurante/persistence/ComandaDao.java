package br.ucsal.bes20202.testequalidade.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20202.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class ComandaDao {

	private static final String MENS_COMANDA_NAO_ENCONTRADA = "Nenhuma comanda encontrada para a mesa(número da mesa = %d).";

	private List<Comanda> itens = new ArrayList<>();

	public void incluir(Comanda comanda) {
		itens.add(comanda);
	}

	public List<Comanda> obterComandasPorNumeroMesa(Integer numeroMesa) throws RegistroNaoEncontrado {
		List<Comanda> comandas = new ArrayList<>();
		for (Comanda comanda : itens) {
			if (numeroMesa.equals(comanda.getMesa().getNumero())) {
				comandas.add(comanda);			}
		}
		if (comandas.isEmpty()) {
			throw new RegistroNaoEncontrado(String.format(MENS_COMANDA_NAO_ENCONTRADA, numeroMesa));
		}
		return comandas;
	}

	public Comanda obterPorCodigo(Integer codigo) throws RegistroNaoEncontrado {
		for (Comanda comanda : itens) {
			if (comanda.getCodigo().equals(codigo)) {
				return comanda;
			}
		}
		throw new RegistroNaoEncontrado(String.format(MENS_COMANDA_NAO_ENCONTRADA, codigo));
	}
}

package br.ucsal.bes20202.testequalidade.restaurante.business;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20202.testequalidade.restaurante.builder.MesaBuilder;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao;
	 * 
	 * 3. NÃO é necesário fazer o mock para o construtor de Comanda;
	 * 
	 * 4. Como o método abrirComanda é void, você deverá usar o verify para se
	 * certificar do sucesso ou não da execução do teste. Não é necessário garantir
	 * que a comanda incluída é para a mesa informada, basta verificar se uma
	 * comanda foi incluída.
	 * 
	 */

	public static MesaBuilder mesaBuilder;
	public static MesaDao mockMesaDao;
	public static ComandaDao mockComandaDao;
	public static RestauranteBO mockRestaurante;

	@BeforeAll
	public static void setup() {
		mesaBuilder = MesaBuilder.mesaDefault();
		mockMesaDao = Mockito.mock(MesaDao.class);
		mockComandaDao = Mockito.mock(ComandaDao.class);
		mockRestaurante = Mockito.mock(RestauranteBO.class);
	}

	@Test
	public void abrirComandaMesaLivre() throws RegistroNaoEncontrado, MesaOcupadaException {
		Mesa mesa = mesaBuilder.withNumero(20).withCapacidade(6).withSituacao(SituacaoMesaEnum.LIVRE).but().build();
		
		Comanda comanda = new Comanda(mesa);
		Mockito.when(mockMesaDao.obterPorNumero(Mockito.any())).thenReturn(mesa);
		
		
		mockComandaDao.incluir(comanda);
		mockRestaurante.abrirComanda(mesa.getNumero());

		//Mockito.verify(mockComandaDao).incluir(comanda);
		Mockito.verify(mockRestaurante).abrirComanda(mesa.getNumero());
	}
}

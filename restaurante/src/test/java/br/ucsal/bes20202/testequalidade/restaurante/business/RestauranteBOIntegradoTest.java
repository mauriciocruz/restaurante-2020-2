package br.ucsal.bes20202.testequalidade.restaurante.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20202.testequalidade.restaurante.builder.MesaBuilder;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.MesaDao;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class RestauranteBOIntegradoTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. Como o método abrirComanda é void, você deverá utilizar o ComandaDAO para
	 * verificar se o método teve sucesso. É necessário verificar se a comanda foi
	 * incluída para a mesa informada.
	 * 
	 */
	
	public static MesaBuilder mesaBuilder;
	public static ComandaDao comandaDAO;
	public static ItemDao itemDAO;
	public static RestauranteBO restauranteBO;
	
	@BeforeAll
	public static void setup() {
		mesaBuilder = MesaBuilder.mesaDefault();
	}
	
	@Test
	public void abrirComandaMesaLivre() throws MesaOcupadaException, RegistroNaoEncontrado {
		ComandaDao comandaDAO = new ComandaDao();
		MesaDao mesaDAO = new MesaDao();
		Mesa mesa = mesaBuilder.withNumero(20).withCapacidade(6).withSituacao(SituacaoMesaEnum.LIVRE).but().build();
		
		Comanda comanda = new Comanda(mesa);
		comandaDAO.incluir(comanda);
		mesaDAO.incluir(mesa);
		
		RestauranteBO restaurante = new RestauranteBO(mesaDAO, new ComandaDao(), new ItemDao());
		restaurante.abrirComanda(mesa.getNumero());
		
		List<Comanda> listaComandas = comandaDAO.obterComandasPorNumeroMesa(mesa.getNumero());
		
		assertEquals(mesa, listaComandas.get(0).getMesa());
	}
}

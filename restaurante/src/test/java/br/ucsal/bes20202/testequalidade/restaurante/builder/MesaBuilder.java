package br.ucsal.bes20202.testequalidade.restaurante.builder;

import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class MesaBuilder {
	public static final Integer NUMERO = 43;
	public static final Integer CAPACIDADE = 10;
	public static final SituacaoMesaEnum SITUACAO = SituacaoMesaEnum.LIVRE;

	private Integer numero = NUMERO;
	private Integer capacidade = CAPACIDADE;
	private SituacaoMesaEnum situacao = SITUACAO;

	private MesaBuilder() {
	}

	public static MesaBuilder mesa() {
		return new MesaBuilder().withNumero(NUMERO).withCapacidade(CAPACIDADE).withSituacao(SITUACAO);
	}

	public static MesaBuilder mesaDefault() {
		return MesaBuilder.mesa();
	}

	public MesaBuilder withNumero(Integer numero) {
		this.numero = numero;
		return this;
	}

	public MesaBuilder withCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
		return this;
	}

	public MesaBuilder withSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public MesaBuilder but() {
return mesa().withNumero(this.numero).withCapacidade(this.capacidade).withSituacao(this.situacao);
	}

	public Mesa build() {
		Mesa instancia = new Mesa();
		instancia.setCapacidade(this.capacidade);
		instancia.setNumero(this.numero);
		instancia.setSituacao(this.situacao);
		return instancia;
		// return new Mesa();
	}
}